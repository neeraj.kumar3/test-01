import '../styles/globals.css';
import '../styles/main.scss';
import ContainerProvider from '../utils/container';

export default function MyApp({ Component, pageProps }) {
  return (
    <ContainerProvider>
      <Component {...pageProps} />
    </ContainerProvider>
  );
}
