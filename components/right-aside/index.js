import { useState } from 'react';
import classNames from 'classnames/bind';
import rightAsideStyles from './right-aside.module.scss';
import Chat from '../chat';
import Question from '../question';
import Tips from '../tips';
import Artist from '../artist';

let cx = classNames.bind(rightAsideStyles);
const RightAside = ({}) => {
  const [currTab, setCurrTab] = useState(1);

  let TabContent = null;
  switch (currTab) {
    case 2: {
      TabContent = <Question />;
      break;
    }
    case 3: {
      TabContent = <Tips />;
      break;
    }
    case 4: {
      TabContent = <Artist />;
      break;
    }
    default: {
      TabContent = <Chat />;
    }
  }

  return (
    <aside className={cx('audience')}>
      <div className={cx('tab-nav')}>
        <ul className="d-flex justify-content-between align-items-center">
          <li>
            <span
              className={cx({ active: currTab === 1 })}
              onClick={() => setCurrTab(1)}
            >
              Chat
            </span>
          </li>
          <li>
            <span
              className={cx({ active: currTab === 2 })}
              onClick={() => setCurrTab(2)}
            >
              Questions
            </span>
          </li>
          <li>
            <span
              className={cx({ active: currTab === 3 })}
              onClick={() => setCurrTab(3)}
            >
              Tips
            </span>
          </li>
          <li>
            <span
              className={cx({ active: currTab === 4 })}
              onClick={() => setCurrTab(4)}
            >
              Artist
            </span>
          </li>
        </ul>
      </div>
      <div className={cx('tab-content')}>{TabContent}</div>
    </aside>
  );
};

export default RightAside;
