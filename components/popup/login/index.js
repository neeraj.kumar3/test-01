// import '../../styles/--scss-vars.scss';
import { useState } from 'react';
import Image from 'next/image';
import classNames from 'classnames/bind';
import popupStyles from './../popup.module.scss';
// import Input from '../form/input';
import inputStyles from '../../form/input/input.module.scss';
import buttonStyles from '../../form/button/button.module.scss';
import { useGlobalMutation } from '../../../utils/container';
import { loginFields } from '../../../utils/jsonData';
let cx1 = classNames.bind(inputStyles);
let cx2 = classNames.bind(buttonStyles);
let cx = classNames.bind(popupStyles);

const initialState = loginFields.reduce((prev, curr) => {
  const result = { ...prev, [curr.name]: curr.value };
  return result;
}, {});

export default function Popup({}) {
  const { updatePopup } = useGlobalMutation();
  const [togglePopup, setTogglePopup] = useState(false);
  const [state, setState] = useState(initialState);

  function handleSubmit() {
    if (Object.values(state).some((item) => item === '')) return;

    console.log(state);
  }

  return (
    <div className={cx('popup')}>
      <div
        onClick={() => setTogglePopup(!togglePopup)}
        className={cx('backoverlay', togglePopup ? 'show' : 'hide')}
      />
      <div className={cx('popup-dailog')}>
        <div className={cx('inner-wrap')}>
          <div className={cx('sec-head')}>
            <h3 className={cx('title')}>login</h3>
            <p>
              or create{' '}
              <span onClick={() => updatePopup('sign-up')}>an account</span>
            </p>
          </div>
          {loginFields.map((field) => (
            <div key={field.name} className={cx1('form-group')}>
              <input
                type={field.type}
                value={state[field.name]}
                onChange={(evt) =>
                  setState((prevState) => ({
                    ...prevState,
                    [field.name]: evt.target.value,
                  }))
                }
                className={cx1('form-control')}
                placeholder={field.placeholder}
              />
            </div>
          ))}
          <div className={cx('btn-wrap')}>
            <button
              type="Submit"
              className={cx2('btn', 'btn-primary')}
              onClick={handleSubmit}
            >
              Next
            </button>
          </div>
          <div className={cx('forgot')}>
            <a href="#">I forgot my username or password </a>
          </div>
        </div>
      </div>
    </div>
  );
}
