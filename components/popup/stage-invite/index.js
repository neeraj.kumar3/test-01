// import '../../styles/--scss-vars.scss';
import { useState } from 'react';
import Image from 'next/image';
import classNames from 'classnames/bind';
import popupStyles from './../popup.module.scss';
import buttonStyles from '../../form/button/button.module.scss';
import { useGlobalMutation } from '../../../utils/container';
let cx2 = classNames.bind(buttonStyles);
let cx = classNames.bind(popupStyles);

export default function StageInvitePopup({}) {
  return (
    <div className={cx('popup', 'stage-invite', 'show')}>
      <div className={cx('backoverlay', 'show')}></div>
      <div className={cx('popup-dailog')}>
        {/* <h3>Thank you for supporting Marco j.!</h3> */}
        <div
          className={cx(
            'inner-wrap',
            'd-flex',
            'flex-wrap',
            'justify-content-start',
          )}
        >
          <div className={cx('image-box')}>
            <img
              src="./assets/images/stage-icon.png"
              className="img-fluid"
              alt=""
            />
          </div>
          <div className={cx('content-box')}>
            <h3>Marco J. is inviting you on stage</h3>
            <p className={cx('desc')}>
              Your camera and mic will be on while onstage.
            </p>
            <div
              className={cx(
                'btn-wrap',
                'd-flex',
                'justify-content-end',
                'align-items-center',
              )}
            >
              <button className={cx2('btn', 'btn-primary', 'lg')}>
                ACCEPT
              </button>
              <button className={cx2('btn', 'btn-secondary', 'lg')}>
                DECLINE
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
