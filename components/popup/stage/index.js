// import '../../styles/--scss-vars.scss';
import { useState } from 'react';
import Image from 'next/image';
import classNames from 'classnames/bind';
import popupStyles from './../popup.module.scss';
import buttonStyles from '../../form/button/button.module.scss';
import { useGlobalMutation } from '../../../utils/container';
let cx2 = classNames.bind(buttonStyles);
let cx = classNames.bind(popupStyles);

export default function StagePopup({}) {
  return (
    <div className={cx('popup', 'stage')}>
      <div className={cx('popup-dailog')}>
        <div className={cx('close')}></div>
        <div
          className={cx(
            'inner-wrap',
            'd-flex',
            'flex-wrap',
            'justify-content-start',
          )}
        >
          <div className={cx('image-box')}>
            <img
              src="./assets/images/stage-icon.png"
              className="img-fluid"
              alt=""
            />
          </div>
          <div className={cx('content-box')}>
            <p>
              Would you like to be eligible to appear on stage to meet the
              artist during this livecast?
            </p>
            <div className={cx('note')}>
              We suggest having a question ready!
            </div>
            <div
              className={cx(
                'btn-wrap',
                'd-flex',
                'justify-content-end',
                'align-items-center',
              )}
            >
              <button className={cx2('btn', 'btn-primary', 'lg')}>
                YES PLEASE
              </button>
              <button className={cx2('btn', 'btn-secondary', 'lg')}>
                NO THANKS
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
