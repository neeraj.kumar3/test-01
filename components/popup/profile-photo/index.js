// import '../../styles/--scss-vars.scss';
import { useState } from 'react';
import Image from 'next/image';
import classNames from 'classnames/bind';
import popupStyles from './../popup.module.scss';
import inputStyles from '../../form/input/input.module.scss';
import buttonStyles from '../../form/button/button.module.scss';
let cx1 = classNames.bind(inputStyles);
let cx2 = classNames.bind(buttonStyles);

let cx = classNames.bind(popupStyles);
const UploadPopup = ({}) => {
  const [togglePopup, setTogglePopup] = useState(false);
  return (
    <div className={cx('popup')}>
      <div
        onClick={() => setTogglePopup(!togglePopup)}
        className={
          togglePopup ? cx('backoverlay', 'show') : cx('backoverlay', 'hide')
        }
      ></div>
      <div className={cx('popup-dailog')}>
        <div className={cx('inner-wrap')}>
          <div className={cx('sec-head')}>
            <h3 className={cx('title')}>
              Add a<br />
              profile photo{' '}
            </h3>
          </div>
          <div className={cx('upload-photo')}>
            <input type="file" id="avtar" />
            <label
              htmlFor="avtar"
              className="d-flex align-items-center justify-content-center"
            >
              <div className={cx('outer')}>
                <div className={cx('icon')}>
                  <img
                    src="./assets/images/camera-icon.png"
                    className="img-fluid"
                  />
                </div>
                <div className={cx('text')}>
                  Upload a picture
                  <br />
                  (optional){' '}
                </div>
              </div>
              {/* <div className={cx('preview')}>
                            <img src='https://via.placeholder.com/150x150' className='img-fluid' />
                        </div> */}
            </label>
          </div>
          <div className={cx('btn-wrap')}>
            <button type="Submit" className={cx2('btn', 'btn-primary')}>
              Submit
            </button>
          </div>
          <div className={cx('forgot')}>
            <a href="#">skip</a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UploadPopup;
