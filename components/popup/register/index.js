// import '../../styles/--scss-vars.scss';
import { useState } from 'react';
import Image from 'next/image';
import classNames from 'classnames/bind';
import popupStyles from './../popup.module.scss';
// import Input from '../form/input';
import inputStyles from '../../form/input/input.module.scss';
import buttonStyles from '../../form/button/button.module.scss';
import { useGlobalMutation } from '../../../utils/container';
let cx1 = classNames.bind(inputStyles);
let cx2 = classNames.bind(buttonStyles);

let cx = classNames.bind(popupStyles);
const RegisterPopup = ({}) => {
  const { updatePopup } = useGlobalMutation();
  const [togglePopup, setTogglePopup] = useState(false);

  return (
    <div className={cx('popup')}>
      <div
        onClick={() => setTogglePopup(!togglePopup)}
        className={
          togglePopup ? cx('backoverlay', 'show') : cx('backoverlay', 'hide')
        }
      ></div>
      <div className={cx('popup-dailog')}>
        <div className={cx('inner-wrap')}>
          <div className={cx('sec-head')}>
            <h3 className={cx('title')}>
              CREATE AN <br /> ACCOUNT
            </h3>
          </div>
          <div className={cx('notification', 'success', 'error')}>
            successfully register
          </div>
          <div className={cx1('form-group')}>
            <input
              type="text"
              className={cx1('form-control')}
              placeholder="Username"
            />
          </div>
          <div className={cx1('form-group')}>
            <input
              type="text"
              className={cx1('form-control')}
              placeholder="Enter your email address"
            />
          </div>
          <div className={cx1('form-group')}>
            <input
              type="password"
              className={cx1('form-control')}
              placeholder="Password must be at least 8 charcters"
            />
          </div>
          <div className={cx1('form-group')}>
            <input
              type="text"
              className={cx1('form-control')}
              placeholder="Cellphone no. ###-###-###"
            />
          </div>
          <p className={cx('info')}>
            * Ursa Live will send you notifications about the artists you
            follow, and occasionally may send you notifications about special
            offers and major events.
          </p>
          <div className={cx('btn-wrap')}>
            <button type="Submit" className={cx2('btn', 'btn-primary')}>
              Next
            </button>
          </div>
          <div className={cx('forgot')}>
            <span className={cx('make-link')} onClick={() => updatePopup('')}>
              cancel
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegisterPopup;
