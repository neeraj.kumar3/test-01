// import '../../styles/--scss-vars.scss';
import Image from 'next/image';
import classNames from 'classnames/bind';
import headerStyles from './header.module.scss';
import { useGlobalMutation } from '../../utils/container';

let cx = classNames.bind(headerStyles);
const Header = ({}) => {
  const { updatePopup } = useGlobalMutation();

  return (
    <header className={cx('header')}>
      <div className="container-fluid">
        <div className="row align-items-center">
          <div className="col-md-6">
            <div className={cx('logo-outer')}>
              <div className={cx('site-logo')}>
                <a href="#">
                  <img
                    src={'/assets/images/logo.png'}
                    className="img-fluid"
                    alt=""
                  />
                </a>
              </div>
              <div className={cx('tagline')}>LIVE WITH MARCO j.</div>
            </div>
          </div>
          <div className="col-md-6">
            <div className={cx('login-links')}>
              <ul className="d-flex justify-content-end align-items-content">
                <li onClick={() => updatePopup('sign-up')}>
                  <span>Sign Up</span>
                </li>
                /
                <li onClick={() => updatePopup('log-in')}>
                  <span>Log In</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
