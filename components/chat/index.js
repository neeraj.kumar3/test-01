// import '../../styles/--scss-vars.scss';
import Image from 'next/image';
import classNames from 'classnames/bind';
import chatStyles from './chat.module.scss';

let cx = classNames.bind(chatStyles);
const list = [
  { _id: 0 },
  { _id: 1 },
  { _id: 2 },
  { _id: 3 },
  { _id: 3 },
  { _id: 3 },
];
const Chat = ({}) => {
  return (
    <div className={cx('chat')}>
      <div className={cx('inner', 'vScroll')}>
        {list.map((item) => (
          <div
            className={cx(
              'item',
              'd-flex',
              'justify-content-start',
              'align-items-start',
            )}
          >
            <div className={cx('icon')}>
              <img src="./assets/images/chat-avtar.png" className="img-fluid" />
            </div>
            <div className={cx('detail')}>
              <div
                className={cx(
                  'head-part',
                  'd-flex',
                  'justify-content-between',
                  'align-items-center',
                )}
              >
                <div
                  className={cx(
                    'left',
                    'd-flex',
                    'justify-content-start',
                    'align-items-center',
                  )}
                >
                  <div className={cx('name')}>Marco J.</div>
                  <div className={cx('time')}>20 sec ago</div>
                </div>
                <div className={cx('right')}>
                  <div className={cx('action')}>
                    <img
                      src="./assets/images/vertical-dots.svg"
                      className="img-fluid"
                      alt=""
                    />
                  </div>
                </div>
              </div>
              <div className={cx('bottom-part')}>
                <p>Cant wait to see you in Chicago! Big fan since 2000!</p>
              </div>
            </div>
          </div>
        ))}
      </div>
      <div className={cx('note')}>
        Questions for the artist? Put them in the “Questions” tab{' '}
        <span className={cx('close')}>&times;</span>
      </div>
      <div
        className={cx(
          'chat-box',
          'd-flex',
          'justify-content-start',
          'align-items-start',
        )}
      >
        <div className={cx('icon')}>
          <img src="./assets/images/chat-avtar.png" className="img-fluid" />
        </div>
        <div
          className={cx(
            'detail',
            'd-flex',
            'justify-content-start',
            'align-items-center',
          )}
        >
          <div className={cx('left')}>
            <div
              className={cx(
                'head-part',
                'd-flex',
                'justify-content-between',
                'align-items-center',
              )}
            >
              <div className={cx('name')}>PaulSings</div>
            </div>
            <div className={cx('bottom-part')}>
              <input type="text" name="" placeholder="Type here..." />
            </div>
          </div>
          <div className={cx('right')}>
            <div className={cx('action')}>
              <img
                src="./assets/images/enter-btn.png"
                className="img-fluid"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Chat;
