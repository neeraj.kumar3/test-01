// import '../../styles/--scss-vars.scss';
import Image from 'next/image';
import classNames from 'classnames/bind';
import tipsStyles from './tips.module.scss';
import buttonStyles from '../form/button/button.module.scss';
let cx1 = classNames.bind(buttonStyles);
let cx = classNames.bind(tipsStyles);

const Tips = ({}) => {
  return (
    <div className={cx('tips')}>
      <div className={cx('winner')}>
        <div className={cx('reward-btn')}>
          <button className={cx1('btn', 'btn-primary')}>Rewards</button>
        </div>
        <div className={cx('total-earn')}>Total earned = 4500 lc</div>
        <div
          className={cx(
            'item',
            'd-flex',
            'justify-content-between',
            'align-items-center',
          )}
        >
          <div
            className={cx(
              'left-box',
              'd-flex',
              'justify-content-start',
              'align-items-center',
            )}
          >
            <div className={cx('count')}>1.</div>
            <div className={cx('avtar')}>
              <img
                src="./assets/images/artist-avtar.png"
                className="img-fluid"
              />
            </div>
            <div className={cx('name')}>Mitt</div>
          </div>
          <div
            className={cx(
              'right-box',
              'd-flex',
              'justify-content-end',
              'align-items-center',
            )}
          >
            <div className={cx('amount')}>1000 lc</div>
            <div className={cx('emoji')}>
              <img
                src="./assets/images/trophy-icon.png"
                className="img-fluid"
              />
            </div>
          </div>
        </div>
        <div
          className={cx(
            'item',
            'd-flex',
            'justify-content-between',
            'align-items-center',
          )}
        >
          <div
            className={cx(
              'left-box',
              'd-flex',
              'justify-content-start',
              'align-items-center',
            )}
          >
            <div className={cx('count')}>1.</div>
            <div className={cx('avtar')}>
              <img
                src="./assets/images/artist-avtar.png"
                className="img-fluid"
              />
            </div>
            <div className={cx('name')}>Mitt</div>
          </div>
          <div
            className={cx(
              'right-box',
              'd-flex',
              'justify-content-end',
              'align-items-center',
            )}
          >
            <div className={cx('amount')}>1000 lc</div>
            <div className={cx('emoji')}>
              <img
                src="./assets/images/trophy-icon.png"
                className="img-fluid"
              />
            </div>
          </div>
        </div>
        <div className={cx('more')}>
          <a href="#">view more</a>
        </div>
      </div>
      <div className={cx('inner', 'vScroll')}>
        <div className={cx('userlist')}>
          <div
            className={cx(
              'item',
              'd-flex',
              'justify-content-between',
              'align-items-center',
            )}
          >
            <div
              className={cx(
                'left-box',
                'd-flex',
                'justify-content-start',
                'align-items-start',
              )}
            >
              <div className={cx('avtar')}>
                <img
                  src="./assets/images/artist-avtar.png"
                  className="img-fluid"
                />
              </div>
              <div className={cx('detail', 'd-flex')}>
                <div className={cx('name')}>Matt</div>
                <div className={cx('time')}>20 sec ago</div>
              </div>
            </div>
            <div
              className={cx(
                'right-box',
                'd-flex',
                'justify-content-end',
                'align-items-center',
              )}
            >
              <div className={cx('amount')}>1000 lc</div>
              <div className={cx('emoji')}>
                <img
                  src="./assets/images/disk-icon.png"
                  className="img-fluid"
                />
              </div>
            </div>
          </div>
          <div
            className={cx(
              'item',
              'd-flex',
              'justify-content-between',
              'align-items-center',
            )}
          >
            <div
              className={cx(
                'left-box',
                'd-flex',
                'justify-content-start',
                'align-items-start',
              )}
            >
              <div className={cx('avtar')}>
                <img
                  src="./assets/images/artist-avtar.png"
                  className="img-fluid"
                />
              </div>
              <div className={cx('detail', 'd-flex')}>
                <div className={cx('name')}>Matt</div>
                <div className={cx('time')}>20 sec ago</div>
              </div>
            </div>
            <div
              className={cx(
                'right-box',
                'd-flex',
                'justify-content-end',
                'align-items-center',
              )}
            >
              <div className={cx('amount')}>1000 lc</div>
              <div className={cx('emoji')}>
                <img
                  src="./assets/images/applause-icon.png"
                  className="img-fluid"
                />
              </div>
            </div>
          </div>
          <div
            className={cx(
              'item',
              'd-flex',
              'justify-content-between',
              'align-items-center',
            )}
          >
            <div
              className={cx(
                'left-box',
                'd-flex',
                'justify-content-start',
                'align-items-start',
              )}
            >
              <div className={cx('avtar')}>
                <img
                  src="./assets/images/artist-avtar.png"
                  className="img-fluid"
                />
              </div>
              <div className={cx('detail', 'd-flex')}>
                <div className={cx('name')}>Matt</div>
                <div className={cx('time')}>20 sec ago</div>
              </div>
            </div>
            <div
              className={cx(
                'right-box',
                'd-flex',
                'justify-content-end',
                'align-items-center',
              )}
            >
              <div className={cx('amount')}>1000 lc</div>
              <div className={cx('emoji')}>
                <img
                  src="./assets/images/fire-icon.png"
                  className="img-fluid"
                />
              </div>
            </div>
          </div>
          <div
            className={cx(
              'item',
              'd-flex',
              'justify-content-between',
              'align-items-center',
            )}
          >
            <div
              className={cx(
                'left-box',
                'd-flex',
                'justify-content-start',
                'align-items-start',
              )}
            >
              <div className={cx('avtar')}>
                <img
                  src="./assets/images/artist-avtar.png"
                  className="img-fluid"
                />
              </div>
              <div className={cx('detail', 'd-flex')}>
                <div className={cx('name')}>Matt</div>
                <div className={cx('time')}>20 sec ago</div>
              </div>
            </div>
            <div
              className={cx(
                'right-box',
                'd-flex',
                'justify-content-end',
                'align-items-center',
              )}
            >
              <div className={cx('amount')}>1000 lc</div>
              <div className={cx('emoji')}>
                <img
                  src="./assets/images/rockon-icon.png"
                  className="img-fluid"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tips;
