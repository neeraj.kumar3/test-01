import Image from 'next/image';
import classNames from 'classnames/bind';
import videoScreenStyles from './video-screen.module.scss';
import popupStyles from '../popup/popup.module.scss';
import RegisterPopup from '../popup/register';
import LoginPopup from '../popup/login';
// import StagePopup from '../popup/stage';
import StageInvitePopup from '../popup/stage-invite';
import { useGlobalState } from '../../utils/container';
// import RegisterPopup from '../popup/profile-photo';
import radioStyles from '../form/radio/radio.module.scss';
import buttonStyles from '../form/button/button.module.scss';
import inputStyles from '../form/input/input.module.scss';

let cx = classNames.bind(videoScreenStyles);
let cx1 = classNames.bind(radioStyles);
let cx2 = classNames.bind(buttonStyles);
let cx3 = classNames.bind(inputStyles);
const list = [
  { _id: 0, eventDate: 'January 8th, 2022 8PM ET' },
  { _id: 1, eventDate: 'January 10th, 2022 8PM ET' },
  { _id: 2, eventDate: 'January 18th, 2022 8PM ET' },
  { _id: 3, eventDate: 'January 25th, 2022 8PM ET' },
  { _id: 4, eventDate: 'January 10th, 2022 8PM ET' },
  { _id: 5, eventDate: 'January 18th, 2022 8PM ET' },
  { _id: 6, eventDate: 'January 25th, 2022 8PM ET' },
];

const EventCard = ({ data }) => (
  <div className={cx('event-card')}>
    <div className={cx('dateTime')}>{data.eventDate}</div>
    <div className={cx('title')}>Add to your calendar:</div>
    <div
      className={cx(
        'calender',
        'd-flex',
        'align-items-center',
        'justify-content-between',
      )}
    >
      <div
        className={cx(
          'item',
          'd-flex',
          'align-items-center',
          'justify-content-start',
        )}
      >
        <div className={cx('icon')}>
          <img src="./assets/images/ical.png" alt="" />
        </div>
        <div className={cx('text')}>iCal</div>
      </div>
      <div
        className={cx(
          'item',
          'd-flex',
          'align-items-center',
          'justify-content-start',
        )}
      >
        <div className={cx('icon')}>
          <img src="./assets/images/gcal.png" alt="" />
        </div>
        <div className={cx('text')}>gCal</div>
      </div>
    </div>
  </div>
);
const Artistdetail = ({ data }) => (
  <div className={cx('artist-window')}>
    <div className={cx('inner-wrap', 'vScroll')}>
      <div className={cx('cast')}>
        <div className={cx('avtar')}>
          <img
            src="./assets/images/artist-avtar.png"
            className="img-fluid"
            alt=""
          />
        </div>
        <div className={cx('info')}>
          <h3 className={cx('name')}>marco j.</h3>
          <p>Upcoming livecasts:</p>
        </div>
      </div>

      <div className={cx('event-wrap')}>
        <div className="row">
          {list.map((item) => (
            <div className="col-lg-3 col-md-4 col-sm-6 col-12" key={item._id}>
              <EventCard data={item} />
            </div>
          ))}
        </div>
      </div>

      {/* <Popup /> */}
      {/* <RegisterPopup /> */}
      <RegisterPopup />
    </div>
  </div>
);
const StageOption = () => (
  <div className={cx('info-dropdown')}>
    <a href="#" className={cx('info-icon')}>
      <img src="./assets/images/horizantal-dots.svg" alt="" />
    </a>
    <div className={cx('dd-card')}>
      <div
        className={cx(
          'dd-head',
          'd-flex',
          'justify-content-between',
          'align-items-center',
        )}
      >
        <div
          className={cx(
            'left',
            'd-flex',
            'justify-content-start',
            'align-items-center',
          )}
        >
          <div className={cx('avtar')}>
            <img src="./assets/images/avtar-img.png" className="img-fluid" />
          </div>
          <div className={cx('name')}>Jessie</div>
        </div>
        <div className={cx('right')}>
          <div className={cx('close')}></div>
        </div>
      </div>
      <div className={cx('item')}>
        <a href="#">Remove from Stage</a>
      </div>
      <div className={cx('item')}>
        <a href="#">Block</a>
      </div>
    </div>
  </div>
);
const HasQuestion = () => (
  <div
    className={cx(
      'hasQuestion',
      'd-flex',
      'justify-content-start',
      'align-items-center',
    )}
  >
    <div className={cx('icon')}>
      <img src="./assets/images/32x32-avtar.png" className="img-fluid" alt="" />
    </div>
    <div className={cx('text')}>
      Juan22 has a question and wants to be pulled on stage to meet you!{' '}
      <span className={cx('red')}>declined</span>
      <span className={cx('close')}></span>
    </div>
  </div>
);

const TipsThankYou = () => (
  <div
    className={cx(
      'tipsthanks',
      'd-flex',
      'justify-content-start',
      'align-items-center',
    )}
  >
    <div className={cx('icon')}>
      <img src="./assets/images/38x38-avtar.png" className="img-fluid" alt="" />
    </div>
    <div className={cx('detail')}>
      <div className={cx('name')}>
        RockHuman22 <span>tipped 10 lc</span>
      </div>
    </div>
    <div className={cx('emoji')}>
      <img
        src="./assets/images/applause-icon.png"
        className="img-fluid"
        alt=""
      />
    </div>
  </div>
);

const Notified = () => (
  <div className={cx('notified')}>
    <div className={cx('title')}>
      Would you like to be notified when this artist goes live?
    </div>
    <div className={cx('optionlist')}>
      <div className={cx('form-inline')}>
        <input
          type="radio"
          name=""
          className={cx1('custom-radio')}
          id="viaSms"
        />
        <label htmlFor="viaSms">via SMS</label>
      </div>
      <div className={cx('form-inline')}>
        <input
          type="radio"
          name=""
          className={cx1('custom-radio')}
          id="viaEmail"
        />
        <label htmlFor="viaEmail">via Email</label>
      </div>
      <div className={cx('form-inline')}>
        <input
          type="radio"
          name=""
          className={cx1('custom-radio')}
          id="noThanks"
        />
        <label htmlFor="noThanks">No thanks</label>
      </div>
    </div>
    <div className={cx('notes')}>
      Eventually you will be able to update these preferences for each artist
      you follow.
    </div>
    <div className={cx('btnWrap')}>
      <button className={cx2('btn', 'btn-primary')}>DONE</button>
    </div>
  </div>
);

const Rewards = () => (
  <div className={cx('rewards')}>
    <div className={cx('title')}>Marco J. Rewards</div>
    <ul className={cx('points')}>
      <li>
        Top Tipper gets a <span className={cx('blue-text')}>merch bundle</span>
      </li>
      <li>2nd place gets a personalized video message</li>
      <li>
        3rd place gets a signed vinyl LP One random tipper will also get a
        signed vinyl LP
      </li>
    </ul>
    <div className={cx('btnWrap')}>
      <button className={cx2('btn', 'btn-primary')}>DONE</button>
    </div>
  </div>
);

const TipAmount = () => (
  <div className={cx('tipamount')}>
    <div className={cx('tiphead')}>
      <div className={cx('title')}>
        TIP AMOUNT <span>10 livecoins (lc) = 1 dollar </span>
      </div>
      <div className={cx('close')}></div>
    </div>
    <div className={cx('tipbody')}>
      <div className={cx('emoji-list', 'd-flex', 'justify-content-start')}>
        <div className={cx('item')}>
          <div className={cx('card')}>
            <div className={cx('icon')}>
              <img
                src="./assets/images/applause-icon.png"
                className="img-fluid"
                alt=""
              />
            </div>
            <div className={cx('text')}>10 lc</div>
          </div>
        </div>
        <div className={cx('item')}>
          <div className={cx('card')}>
            <div className={cx('icon')}>
              <img
                src="./assets/images/rockon-icon.png"
                className="img-fluid"
                alt=""
              />
            </div>
            <div className={cx('text')}>20 lc</div>
          </div>
        </div>
        <div className={cx('item')}>
          <div className={cx('card')}>
            <div className={cx('icon')}>
              <img
                src="./assets/images/fire-icon.png"
                className="img-fluid"
                alt=""
              />
            </div>
            <div className={cx('text')}>50 lc</div>
          </div>
        </div>
        <div className={cx('item')}>
          <div className={cx('card')}>
            <div className={cx('icon')}>
              <img
                src="./assets/images/disk-icon.png"
                className="img-fluid"
                alt=""
              />
            </div>
            <div className={cx('text')}>100 lc</div>
          </div>
        </div>
        <div className={cx('item')}>
          <div className={cx('card')}>
            <div className={cx('icon')}>
              <img
                src="./assets/images/trophy-icon.png"
                className="img-fluid"
                alt=""
              />
            </div>
            <div className={cx('text')}>200 lc</div>
          </div>
        </div>
      </div>
      <div className={cx('form-group')}>
        <input
          type="text"
          className={cx3('form-control', 'lg')}
          id=""
          placeholder="enter your own amount"
        />
      </div>
      <div className={cx('lc-list')}>
        <div className={cx('item')}>
          <button className={cx2('btn', 'btn-gray-outline')}>10 lc</button>
        </div>
        <div className={cx('item')}>
          <button className={cx2('btn', 'btn-gray-outline')}>20 lc</button>
        </div>
        <div className={cx('item')}>
          <button className={cx2('btn', 'btn-gray-outline')}>50 lc</button>
        </div>
        <div className={cx('item')}>
          <button className={cx2('btn', 'btn-gray-outline')}>100 lc</button>
        </div>
        <div className={cx('item')}>
          <button className={cx2('btn', 'btn-gray-outline')}>200 lc</button>
        </div>
      </div>
    </div>
    <div
      className={cx(
        'btnWrap',
        'd-flex',
        'justify-content-between',
        'align-items-center',
      )}
    >
      <div className={cx('left')}>
        <button className={cx2('btn', 'btn-primary')}>SUBMIT</button>
      </div>
      <div className={cx('right')}>
        <b>
          <a href="#">BUY MORE</a>
        </b>
        livecoins In your wallet: 200 livecoins
      </div>
    </div>
  </div>
);

const VideoScreen = () => {
  const { popup } = useGlobalState();
  // sprite class add on videoScreen to show two video
  return (
    <div className={cx('videoScreen')}>
      {/* <Artistdetail /> */}
      <div className={cx('artist-video')}>
        <img src="./assets/images/artist-video.jpg" className="img-fluid" />
        {/* <HasQuestion /> */}
        {/* <TipsThankYou /> */}
        {/* <Notified /> */}
        {/* <TipAmount /> */}
        {/* <StagePopup /> */}
        {/* <StageInvitePopup /> */}
        <Rewards />
      </div>
      {/* <div className={cx('audience-video')}>
        <img src='./assets/images/artist-video.jpg' className='img-fluid' />
        <StageOption />
       </div> */}
      {popup === 'log-in' && <LoginPopup />}
      {popup === 'sign-up' && <RegisterPopup />}
    </div>
  );
};

export default VideoScreen;
