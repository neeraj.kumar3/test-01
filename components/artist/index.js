// import '../../styles/--scss-vars.scss';
import Image from 'next/image';
import classNames from 'classnames/bind';
import artistStyles from './artist.module.scss';

let cx = classNames.bind(artistStyles);
const list = [
  { _id: 0 },
  { _id: 1 },
  { _id: 2 },
  { _id: 3 },
  { _id: 3 },
  { _id: 3 },
];
const Artist = ({}) => {
  return (
    <div className={cx('artist')}>
      <div className={cx('inner', 'vScroll')}>
        <div className={cx('profile')}>
          <div className={cx('about')}>
            <div className={cx('image-box')}>
              <img
                src="./assets/images/profile-img.png"
                className="img-fluid"
                alt=""
              />
            </div>
            <div className={cx('content-box')}>
              <div className={cx('title')}>Marco J.</div>
              <div className={cx('genre')}>Genre: Lorem ipsum</div>
            </div>
          </div>
          <div className={cx('bio')}>
            <div className={cx('title')}>Bio</div>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
              et elementum nunc, id ullamcorper ante. Pellentesque finibus
              bibendum rhoncus. Nullam gravida mollis malesuada.
              <a href="#">View More</a>
            </p>
          </div>
        </div>
        <div className={cx('livecast')}>
          <div className={cx('title')}>Upcoming livecasts</div>
          <div className={cx('event')}>
            <div className={cx('dateTime')}>January 8th, 2021, 8pm ET</div>

            <div
              className={cx(
                'calender',
                'd-flex',
                'align-items-center',
                'justify-content-start',
              )}
            >
              <div className={cx('text')}>Add to your calendar:</div>
              <div
                className={cx(
                  'item',
                  'd-flex',
                  'align-items-center',
                  'justify-content-start',
                )}
              >
                <div className={cx('icon')}>
                  <img src="./assets/images/ical.png" alt="" />
                </div>
                <div className={cx('text')}>iCal</div>
              </div>
              <div
                className={cx(
                  'item',
                  'd-flex',
                  'align-items-center',
                  'justify-content-start',
                )}
              >
                <div className={cx('icon')}>
                  <img src="./assets/images/gcal.png" alt="" />
                </div>
                <div className={cx('text')}>gCal</div>
              </div>
            </div>
          </div>
          <div className={cx('event')}>
            <div className={cx('dateTime')}>January 8th, 2021, 8pm ET</div>

            <div
              className={cx(
                'calender',
                'd-flex',
                'align-items-center',
                'justify-content-start',
              )}
            >
              <div className={cx('text')}>Add to your calendar:</div>
              <div
                className={cx(
                  'item',
                  'd-flex',
                  'align-items-center',
                  'justify-content-start',
                )}
              >
                <div className={cx('icon')}>
                  <img src="./assets/images/ical.png" alt="" />
                </div>
                <div className={cx('text')}>iCal</div>
              </div>
              <div
                className={cx(
                  'item',
                  'd-flex',
                  'align-items-center',
                  'justify-content-start',
                )}
              >
                <div className={cx('icon')}>
                  <img src="./assets/images/gcal.png" alt="" />
                </div>
                <div className={cx('text')}>gCal</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Artist;
