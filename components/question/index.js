// import '../../styles/--scss-vars.scss';
import Image from 'next/image';
import classNames from 'classnames/bind';
import questionStyles from './question.module.scss';

let cx = classNames.bind(questionStyles);
const list = [
  { _id: 0 },
  { _id: 1 },
  { _id: 2 },
  { _id: 3 },
  { _id: 3 },
  { _id: 3 },
];
const Question = ({}) => {
  return (
    <div className={cx('question')}>
      <div className={cx('inner', 'vScroll')}>
        {list.map((item) => (
          <div
            className={cx(
              'item',
              'd-flex',
              'justify-content-start',
              'align-items-start',
            )}
          >
            <div className={cx('icon')}>
              <img src="./assets/images/chat-avtar.png" className="img-fluid" />
            </div>
            <div className={cx('detail')}>
              <div
                className={cx(
                  'head-part',
                  'd-flex',
                  'justify-content-between',
                  'align-items-start',
                )}
              >
                <div className={cx('left')}>
                  <div
                    className={cx(
                      'outer',
                      'd-flex',
                      'justify-content-start',
                      'align-items-center',
                    )}
                  >
                    <div className={cx('name')}>Marco J.</div>
                    <div className={cx('time')}>20 sec ago</div>
                  </div>
                  <p>Cant wait to see you in Chicago! Big fan since 2000!</p>
                </div>
                <div className={cx('right')}>
                  <div className={cx('action')}>
                    <img
                      src="./assets/images/vertical-dots.svg"
                      className="img-fluid"
                      alt=""
                    />
                  </div>
                </div>
              </div>
              <div className={cx('bottom-part')}>
                <div
                  className={cx(
                    'votes',
                    'active',
                    'd-flex',
                    'align-items-center',
                    'justify-content-start',
                  )}
                >
                  <div className={cx('icon')}>
                    <img src="./assets/images/circle-top-arrow.svg" alt="" />
                  </div>
                  <div className={cx('text')}>300 upvotes</div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Question;
