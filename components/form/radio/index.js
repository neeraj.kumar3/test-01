import Radio from './radio';
import radioStyles from './radio.module.scss';
import classNames from 'classnames/bind';

let cx = classNames.bind(radioStyles);
export default Radio;
