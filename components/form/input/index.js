import Input from './input';
import inputStyles from './input.module.scss';
import classNames from 'classnames/bind';

let cx = classNames.bind(inputStyles);
export default Input;
