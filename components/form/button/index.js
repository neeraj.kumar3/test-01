import Button from './button';
import buttonStyles from './button.module.scss';
import classNames from 'classnames/bind';

let cx = classNames.bind(buttonStyles);
export default Button;
