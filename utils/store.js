const readDefaultState = () => {
  try {
    return JSON.parse(window.sessionStorage.getItem('agora'));
  } catch (err) {
    return {};
  }
};

const defaultState = {
  // web sdk params
  config: {
    uid: 0,
    host: true,
    channelName: '',
    token: '',
    resolution: '480p',
    ...readDefaultState(),
    microphoneId: '',
    cameraId: '',
  },
  popup: '',
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'config': {
      return { ...state, config: action.payload };
    }
    case 'popup': {
      return { ...state, popup: action.payload };
    }
    default:
      throw new Error('mutation type not defined');
  }
};

export { reducer, defaultState };
