export const loginFields = [
  {
    name: 'username',
    type: 'text',
    value: '',
    placeholder: 'Username or email',
  },
  {
    name: 'password',
    type: 'password',
    value: '',
    placeholder: 'Password',
  },
];
