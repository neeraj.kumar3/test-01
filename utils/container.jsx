import { createContext, useContext, useReducer, useEffect } from 'react';
import { reducer, defaultState } from './store';

const StateContext = createContext({});
const MutationContext = createContext({});

export default function ContainerProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, defaultState);

  const methods = {
    updateConfig(params) {
      dispatch({ type: 'config', payload: { ...state.config, ...params } });
    },
    updatePopup(type) {
      console.log('test the flow:---------', type)
      dispatch({ type: 'popup', payload: type });
    },
  };

  useEffect(() => {
    window.sessionStorage.setItem(
      'agora',
      JSON.stringify({
        uid: state.config.uid,
        host: state.config.host,
        channelName: state.config.channelName,
        token: state.config.token,
        resolution: state.config.resolution,
      }),
    );
  }, [state]);

  return (
    <StateContext.Provider value={state}>
      <MutationContext.Provider value={methods}>
        {children}
      </MutationContext.Provider>
    </StateContext.Provider>
  );
}

export function useGlobalState() {
  return useContext(StateContext);
}

export function useGlobalMutation() {
  return useContext(MutationContext);
}
